
import React, {Component} from 'react';
import {Root} from 'native-base';
import {RootNavagator} from './src/routs';
export default class App extends Component {
  render() {
    return (
      <Root>
        <RootNavagator />
      </Root>
    );
  }
}

