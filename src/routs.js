import {
    createStackNavigator
} from 'react-navigation';

import { 
    Home,
    SideBar,
    LoginComponent,
    RegisterComponent,
    FilterComponent,
    ProductListComponent,
    ProductDetailsComponent,
    SpecificationsComponent,
    ProfileComponent,
} from './components';

const RootNavagator = createStackNavigator({
    Home:{screen:Home},
    SideBar:{screen:SideBar},
    Login:{screen:LoginComponent},
    Register:{screen:RegisterComponent},
    Filter:{screen:FilterComponent},
    ProductList:{screen:ProductListComponent},
    ProductDetails:{screen:ProductDetailsComponent},
    Specifications:{screen:SpecificationsComponent},
    Profile:{screen:ProfileComponent},
},{
    headerMode:'none',
    mode:'modal'
})

export {RootNavagator}