import React, { Component } from 'react';
import { StyleSheet} from 'react-native';
import { Button, Footer, FooterTab, Icon } from 'native-base';
class FooterComponent extends Component {
    constructor(props){
        super(props);
    }
    render() {
        return (
            <Footer>
                <FooterTab>
                    <Button>
                        <Icon name="user" type="AntDesign" style={[styles.IconStyle,{}]}/>
                    </Button>
                </FooterTab>
                <FooterTab>
                    <Button>
                        <Icon name="hearto" type="AntDesign" style={[styles.IconStyle,{}]}/>
                    </Button>
                </FooterTab>
                <FooterTab>
                    <Button>
                        <Icon name="search1" type="AntDesign" style={[styles.IconStyle,{}]}/>
                    </Button>
                </FooterTab>
                <FooterTab>
                    <Button>
                        <Icon name="home" type="AntDesign" style={[styles.IconStyle,{}]}/>
                    </Button>
                </FooterTab>
            </Footer>
        );
    }
}

const styles = StyleSheet.create({
    IconStyle:{
		color:'#88296e',
		fontSize:30
	}
})

export {FooterComponent};