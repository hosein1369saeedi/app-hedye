
import React, {Component} from 'react';
import {Image,StyleSheet} from 'react-native';
import {Drawer, Header, Text, Left, Body, Right, Button, Icon, Title, } from 'native-base';
import {SideBar} from './index';
class HeaderHomeComponent extends Component {
  closeDrawer=()=>{
    this.drawer._root.close()
  }
  openDrawer=()=>{
    this.drawer._root.open()
  }
  render() {
    const {navigate}=this.props.navigation;
    return (
      <Drawer
        ref={(ref)=>{this.drawer=ref;}}
        content={<SideBar navigate={navigate}/>}
        onClose={()=>this.closeDrawer()}
      >
        <Header searchBar rounded style={[styles.HeaderStyle,{}]}>
          <Left>
            <Button transparent onPress={()=>this.openDrawer()}>
              <Icon name="navicon" type="FontAwesome" style={[styles.DerawerStyle]}/>
            </Button>
          </Left>
          <Body>
            <Title style={[styles.TitleStyle,{}]}>سامانه آنلاین</Title>
            <Text style={[styles.TextStyle,{}]}>سفارش هدیه و گل در ایران</Text>
          </Body>
          <Right>
            <Image style={{width:50,height:45,}} source={require('../img/logo/logooo.png')}/>
          </Right>
        </Header>
      </Drawer>
    );
  }
}
const styles = StyleSheet.create({
	HeaderStyle:{
		height:70
	},
	DerawerStyle:{
		color:'#b361a0',
		fontSize:37,
	},
	TitleStyle:{
		fontWeight:'bold',
		color:'#4a3199',
	},
	TextStyle:{
		fontSize:11.5,
		color:'#b361a0',
	},
})
export {HeaderHomeComponent}
