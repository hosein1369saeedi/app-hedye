
import React, {Component} from 'react';
import {StyleSheet, Image, ImageBackground, View, } from 'react-native';
import {} from 'react-native-elements';
import {Container, Header, Left, Body, Right, Icon, Title, Text, Content, Footer, FooterTab, Input, Form, Item, Button} from 'native-base';
import ModalSelector from 'react-native-modal-selector';

class LoginComponent extends Component {
	render() {
		return (
			<Container>
				<Header>
					<Left>
						<Button transparent onPress={()=>this.props.navigation.goBack()}>
							<Icon style={{color:'#b361a0'}} name="arrowleft" size={30} type="AntDesign"/>
						</Button>
					</Left>
					<Body>
						<Title style={[styles.TitleStyle,{}]}>سامانه آنلاین</Title>
						<Text style={[styles.TextStyle,{}]}>سفارش هدیه و گل در ایران</Text>
					</Body>
					<Right>
						<Image style={{width:50,height:45,}} source={require('../img/logo/logooo.png')}/>
					</Right>
				</Header>
				<Content>
					<ImageBackground source={require('../img/register-login.png')} style={[styles.HeaderStyle,{}]}>

					</ImageBackground>
					<Text style={[styles.TextFormStyle,{}]}>
						فرم ورود
					</Text>
					<Form style={[styles.FormStyle,{}]}>
						<Item rounded style={[styles.ItemStyle,{}]}>
							<Input rounded placeholder="شماره همراه" style={[styles.InputStyle,{}]}/>
						</Item>
						<Button rounded full style={{borderRadius:20,backgroundColor:'green',top:5,}}>
							<Text>ورود</Text>
						</Button>
					</Form>
				</Content>
				<Footer>
					<FooterTab>
						<Button>
							<Title style={{color:'#999'}}>در صورتی که قبلا ثبت نام نکرده اید <Text style={{color:'red',fontWeight:'bold',}}>اینجا</Text> کلیک کنید</Title>
						</Button>
					</FooterTab>
				</Footer>
			</Container>
		);
	}
}

const styles = StyleSheet.create({
	TitleStyle:{
		fontWeight:'bold',
		color:'#4a3199',
	},
	TextStyle:{
		fontSize:11.5,
		color:'#b361a0',
	},
	HeaderStyle:{
		width:'100%',
		height:180,
		top: 10,
	},
	FormStyle:{
		width:'100%',
		direction:'rtl',
		textAlign:'right',
		paddingHorizontal:30,
		top: 10,
	},
	ItemStyle:{
		marginVertical:5,
		paddingHorizontal:15,
	},
	InputStyle:{
		textAlign:'right',
	},
	TextFormStyle:{
		textAlign:'center',
		paddingVertical:10,
		fontWeight:'bold',
		color:'#fff',
		marginTop: 30,
		backgroundColor:'#b361a0'
	},
})

export {LoginComponent}