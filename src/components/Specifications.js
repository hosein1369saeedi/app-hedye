
import React, {Component} from 'react';
import {StyleSheet, Image, ImageBackground, View, } from 'react-native';
import {} from 'react-native-elements';
import {Container, Header, Left, Body, Right, Icon, Title, Text, Content, Separator, ListItem, Button} from 'native-base';
import ModalSelector from 'react-native-modal-selector';

class SpecificationsComponent extends Component {
	render() {
		return (
			<Container>
				<Header>
					<Left>
						<Button transparent onPress={()=>this.props.navigation.goBack()}>
							<Icon style={{color:'#b361a0'}} name="arrowleft" size={30} type="AntDesign"/>
						</Button>
					</Left>
					<Body>
						<Title style={[styles.TitleStyle,{}]}>سامانه آنلاین</Title>
						<Text style={[styles.TextStyle,{}]}>سفارش هدیه و گل در ایران</Text>
					</Body>
					<Right>
						<Image style={{width:50,height:45,}} source={require('../img/logo/logooo.png')}/>
					</Right>
				</Header>
				<Content>
					<View>
						<Title style={[styles.TitleContentStyle,{}]}>
							ساعت هوشمند گارمین
						</Title>
						<Separator bordered style={styles.SeparatorStyle}>
							<Text style={[styles.SeparatorText,{}]}>مشخصات ظاهری</Text>
						</Separator>
						<Text style={[styles.TextContentStyle,{}]}>مناسب برای : <Text style={{textAlign:'left'}}>آقایان و خانوم ها</Text></Text>
						<Text style={[styles.TextContentStyle,{}]}>Lee Allen</Text>
						<Separator bordered style={styles.SeparatorStyle}>
							<Text style={[styles.SeparatorText,{}]}>MIDFIELD</Text>
						</Separator>
						<Text style={[styles.TextContentStyle,{}]}>Caroline Aaron</Text>
						<Text style={[styles.TextContentStyle,{}]}>Lee Allen</Text>
					</View>
				</Content>
			</Container>
		);
	}
}

const styles = StyleSheet.create({
	TitleStyle:{
		fontWeight:'bold',
		color:'#4a3199',
	},
	TextStyle:{
		fontSize:11.5,
		color:'#b361a0',
	},
	TitleContentStyle:{
		paddingBottom:10,
		paddingTop:11,
		backgroundColor:'#b361a0',
		textAlign:'center',
		color:'#fff',
	},
	SeparatorText:{
		textAlign:'right',
		marginHorizontal:10,
	},
	TextContentStyle:{
		marginVertical:10,
		marginHorizontal:10,
		textAlign:'right'
	},
	SeparatorStyle:{
		height:50
	}
})

export {SpecificationsComponent}