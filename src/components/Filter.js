
import React, {Component} from 'react';
import {StyleSheet, Image, ImageBackground, View, } from 'react-native';
import {} from 'react-native-elements';
import {Container, Header, Left, Body, Button, Right, Icon, Title, Text, Content, Footer, FooterTab, Input, Form, Item,} from 'native-base';
import ModalSelector from 'react-native-modal-selector';
import { ProgressSteps, ProgressStep, } from 'react-native-progress-steps';


class FilterComponent extends Component {
	constructor(props) {
        super(props);
        this.state = {
            textInputValue: ''
        }
    }
	onValueChange2(value: string) {
		this.setState({
			selected2: value
		});
	}
	render() {
		let index = 0;
		const character = [
            { key: index++, label: 'خانه دار'},
            { key: index++, label: 'علاقه مند به آشپزی'},
            { key: index++, label: 'ورزشکار'},
            { key: index++, label: 'مد و فشن'},
            { key: index++, label: 'مذهبی'},
            { key: index++, label: 'اهل تزئینات منزل'},
            { key: index++, label: 'خلاق'},
            { key: index++, label: 'اهل فیلم'},
            { key: index++, label: 'اهل بازی'},
            { key: index++, label: 'اهل هنر'},
        ];
        const Job = [
            { key: index++, label: 'پزشک'},
            { key: index++, label: 'آرتیست'},
            { key: index++, label: 'نقاش'},
            { key: index++, label: 'گرافیست'},
            { key: index++, label: 'تستی'}
        ];
        const birthmonth = [
            { key: index++, label: 'فروردین'},
            { key: index++, label: 'اردیبهشت'},
            { key: index++, label: 'خرداد'},
            { key: index++, label: 'تیر'},
            { key: index++, label: 'مرداد'},
            { key: index++, label: 'شهریور'},
            { key: index++, label: 'مهر'},
            { key: index++, label: 'آبان'},
            { key: index++, label: 'آدر'},
            { key: index++, label: 'دی'},
            { key: index++, label: 'بهمن'},
            { key: index++, label: 'اسفند'},
        ];
        const agerange = [
            { key: index++, label: '۱ تا ۵ سال'},
            { key: index++, label: '۵ تا ۱۰ سال'},
            { key: index++, label: '۱۰ تا ۱۵ سال'},
            { key: index++, label: '۱۵ تا ۲۰ سال'},
            { key: index++, label: '۲۰ تا ۲۵ سال'},
            { key: index++, label: '۲۵ تا ۳۰ سال'},
            { key: index++, label: '۳۰ تا ۴۰ سال'},
            { key: index++, label: '۴۰ تا ۵۰ سال'},
            { key: index++, label: '۵۰ تا ۶۰ سال'},
            { key: index++, label: '۶۰ تا ۷۰ سال'},
            { key: index++, label: '۷۰ تا ۸۰ سال'},
        ];
        const amount = [
            { key: index++, label: 'تا ۵۰ هزار تومان'},
            { key: index++, label: '۵۰ تا ۱۰۰ هزار تومان'},
            { key: index++, label: '۱۰۰ تا ۱۵۰ هزار تومان'},
            { key: index++, label: '۱۵۰ تا ۲۰۰ هزار تومان'},
            { key: index++, label: '۲۰۰ تا ۳۰۰ هزار تومان'},
            { key: index++, label: '۳۰۰ تا ۴۰۰ هزار تومان'},
            { key: index++, label: '۴۰۰ تا ۵۰۰ هزار تومان'},
            { key: index++, label: '۵۰۰ هزار تومان به بالا'},
        ];
        const occasion = [
            { key: index++, label: 'تولد'},
            { key: index++, label: 'ازدواج'},
            { key: index++, label: 'ولنتاین'},
            { key: index++, label: 'روز معلم'},
            { key: index++, label: 'خیریه برای سلامتی'},
            { key: index++, label: 'بی مناسبت'},
            { key: index++, label: 'روز عشق'},
            { key: index++, label: 'جشن نوروز'},
            { key: index++, label: 'جشن کریسمس'},
        ];
		return (
			<Container>
				<Header>
					<Left>
						<Button transparent onPress={()=>this.props.navigation.goBack()}>
							<Icon style={{color:'#b361a0'}} name="arrowleft" size={30} type="AntDesign"/>
						</Button>
					</Left>
					<Body>
						<Title style={[styles.TitleStyle,{}]}>سامانه آنلاین</Title>
						<Text style={[styles.TextStyle,{}]}>سفارش هدیه و گل در ایران</Text>
					</Body>
					<Right>
						<Image style={{width:50,height:45,}} source={require('../img/logo/logooo.png')}/>
					</Right>
				</Header>
				<Content>
					<Text style={[styles.TextFormStyle,{}]}>
						فیلتر جستجوی محصول
					</Text>
					<View style={{direction:'rtl',paddingHorizontal:5}}>
						<ProgressSteps
							style={[styles.ProgressStepsStyle]}
							borderWidth
						>
							<ProgressStep 
								nextBtnText="بعدی"
								nextBtnTextStyle={buttonTextStyle}
								nextBtnStyle={buttonStyle}
								
							>
								<ModalSelector
									rounded
									data={character}
									initValue="انتخاب شخصیت"
									style={[styles.ModalSelector]}
									initValueTextStyle={{color:'#000'}}
									cancelText="انصراف"
								/>
								<ImageBackground source={require('../img/filterscharacter.png')} style={[styles.HeaderStyle,{}]}></ImageBackground>
							</ProgressStep>
							<ProgressStep 
								previousBtnText="قبلی" 
								nextBtnText="بعدی"
								nextBtnTextStyle={buttonTextStyle}
								previousBtnTextStyle={buttonTextStyle}
								nextBtnStyle={buttonStyle}
								previousBtnStyle={buttonStyle}
							>
								<ModalSelector
									data={Job}
									initValue="انتخاب شغل"
									style={[styles.ModalSelector,{}]}
									initValueTextStyle={{color:'#000'}}
									cancelText="انصراف"
								/>
								<ImageBackground source={require('../img/filterjob.png')} style={[styles.HeaderStyle,{}]}></ImageBackground>
							</ProgressStep>
							<ProgressStep 
								previousBtnText="قبلی" 
								nextBtnText="بعدی"
								nextBtnTextStyle={buttonTextStyle} 
								previousBtnTextStyle={buttonTextStyle}
								nextBtnStyle={buttonStyle}
								previousBtnStyle={buttonStyle}
							>
								<ModalSelector
									data={birthmonth}
									initValue="انتخاب ماه تولد"
									style={[styles.ModalSelector,{}]}
									initValueTextStyle={{color:'#000'}}
									cancelText="انصراف"
								/>
								<ImageBackground source={require('../img/filterscalender.png')} style={[styles.HeaderStyle,{}]}></ImageBackground>
							</ProgressStep>
							<ProgressStep 
								previousBtnText="قبلی" 
								nextBtnText="بعدی"
								nextBtnTextStyle={buttonTextStyle} 
								previousBtnTextStyle={buttonTextStyle}
								nextBtnStyle={buttonStyle}
								previousBtnStyle={buttonStyle}
							>
								<ModalSelector
									data={agerange}
									initValue="انتخاب محدوده سنی"
									style={[styles.ModalSelector,{}]}
									initValueTextStyle={{color:'#000'}}
									cancelText="انصراف"
								/>
								<ImageBackground source={require('../img/filtersage.png')} style={[styles.HeaderStyle,{}]}></ImageBackground>
							</ProgressStep>
							<ProgressStep 
								previousBtnText="قبلی" 
								nextBtnText="بعدی"
								nextBtnTextStyle={buttonTextStyle} 
								previousBtnTextStyle={buttonTextStyle}
								nextBtnStyle={buttonStyle}
								previousBtnStyle={buttonStyle}
							>
								<ModalSelector
									data={amount}
									initValue="انتخاب میزان مبلغ هدیه"
									style={[styles.ModalSelector,{}]}
									initValueTextStyle={{color:'#000'}}
									cancelText="انصراف"
								/>
								<ImageBackground source={require('../img/filtersamount.png')} style={[styles.HeaderStyle,{}]}></ImageBackground>
							</ProgressStep>
							<ProgressStep 
								previousBtnText="قبلی" 
								finishBtnText="مشاهده نتایج"
								nextBtnTextStyle={buttonTextStyle}
								previousBtnTextStyle={buttonTextStyle}
								nextBtnStyle={buttonnnStyle}
								previousBtnStyle={buttonStyle}
							>
								<ModalSelector
									data={occasion}
									initValue="انتخاب مناسبت هدیه"
									style={[styles.ModalSelector,{}]}
									initValueTextStyle={{color:'#000'}}
									cancelText="انصراف"
								/>
								<ImageBackground source={require('../img/filtersoccasion.png')} style={[styles.HeaderStyle,{}]}></ImageBackground>
							</ProgressStep>
						</ProgressSteps>
					</View>
				</Content>
				<Footer>
                    <FooterTab>
                        <Button>
                            <Icon name="user" type="AntDesign" style={[styles.IconStyle,{}]}/>
                        </Button>
                    </FooterTab>
                    <FooterTab>
                        <Button>
                            <Icon name="hearto" type="AntDesign" style={[styles.IconStyle,{}]}/>
                        </Button>
                    </FooterTab>
                    <FooterTab>
                        <Button>
                            <Icon name="search1" type="AntDesign" style={[styles.IconStyle,{}]}/>
                        </Button>
                    </FooterTab>
                    <FooterTab>
                        <Button>
                            <Icon name="home" type="AntDesign" style={[styles.IconStyle,{}]}/>
                        </Button>
                    </FooterTab>
                </Footer>
			</Container>
		);
	}
}


const styles = StyleSheet.create({
	TitleStyle:{
		fontWeight:'bold',
		color:'#4a3199',
	},
	TextStyle:{
		fontSize:11.5,
		color:'#b361a0',
	},
	HeaderStyle:{
		width:'100%',
		height:200,
		top: 10,
	},
	ProgressStepsStyle:{
		direction:'rtl',
	},
	FormStyle:{
		width:'100%',
		direction:'rtl',
		textAlign:'right',
		paddingHorizontal:30
	},
	ItemStyle:{
		marginVertical:5,
		paddingHorizontal:15
	},
	InputStyle:{
		textAlign:'right'
	},
	TextFormStyle:{
		textAlign:'center',
		paddingVertical:10,
		fontWeight:'bold',
		color:'#fff',
		marginTop:0,
		backgroundColor:'#b361a0'
	},
	ModalSelector:{
        marginHorizontal:30,
		marginVertical:10,
		direction:'ltr',
    },
    IconStyle:{
		color:'#88296e',
		fontSize:30
	}
})
const buttonTextStyle={
	color:'#fff',
}
const buttonStyle={
	backgroundColor:'#b361a0',
	borderRadius:10,
	paddingHorizontal:20,
	paddingTop:2,
}
const buttonnnStyle={
	backgroundColor:'green',
	borderRadius:10,
	paddingHorizontal:20,
	paddingTop:2,
}

export {FilterComponent}