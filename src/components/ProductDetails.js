
import React, {Component} from 'react';
import {StyleSheet, Image, ImageBackground, View, } from 'react-native';
import {Divider,} from 'react-native-elements';
import {Container, Header, Left, Body, Right, Icon, Title, Text, Content, Footer, FooterTab, Col, Button, Fab, Card, Grid, List, ListItem} from 'native-base';
import ModalSelector from 'react-native-modal-selector';

class ProductDetailsComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
			active: false,
        };
	}	  
	render() {	
		let index = 0;
		const color = [
			{ key: index++, section: true, label: 'رنگ های موجود' },
            { key: index++, label: 'قرمز',component: <View style={[styles.colorStyle,{backgroundColor:'red'}]}><Text style={[styles.textColorStyle,{}]}>قرمز</Text></View>},
            { key: index++, label: 'آبی',component: <View style={[styles.colorStyle,{backgroundColor:'blue'}]}><Text style={[styles.textColorStyle,{}]}>آبی</Text></View>},
            { key: index++, label: 'مشکی',component: <View style={[styles.colorStyle,{backgroundColor:'black'}]}><Text style={[styles.textColorStyle,{}]}>مشکی</Text></View>},
		];
		const size = [
			{ key: index++, section: true, label: 'سایزهای موجود' },
            { key: index++, label: '۳۵',},
            { key: index++, label: '۳۸',},
            { key: index++, label: '۴۲',},
            { key: index++, label: '۴۵',},
		];
		return (
			<Container>
				<Header>
					<Left>
						<Button transparent onPress={()=>this.props.navigation.goBack()}>
							<Icon style={{color:'#b361a0'}} name="arrowleft" size={30} type="AntDesign"/>
						</Button>
					</Left>
					<Body>
						<Title style={[styles.TitleStyle,{}]}>سامانه آنلاین</Title>
						<Text style={[styles.TextStyle,{}]}>سفارش هدیه و گل در ایران</Text>
					</Body>
					<Right>
						<Image style={{width:50,height:45,}} source={require('../img/logo/logooo.png')}/>
					</Right>
				</Header>
				<Content>
                <Card>
					<ImageBackground source={require('../img/product/perfumedetails.png')} style={[styles.HeaderStyle,{paddingHorizontal:5}]}>
						<Fab
							active={this.state.active}
							direction="down"
							containerStyle={{}}
							style={{ backgroundColor:'#000'}}
							position="topRight"
							onPress={() => this.setState({ active: !this.state.active })}
						>
							<Icon name="sharealt" type="AntDesign" style={{color:'#fff'}} />
							<Button style={{ backgroundColor: '#34A34F' }}>
								<Icon name="whatsapp" type="FontAwesome" style={{color:'#fff'}} />
							</Button>
							<Button style={{ backgroundColor: '#3B5998' }}>
								<Icon name="facebook" type="Feather" style={{color:'#fff'}} />
							</Button>
							<Button style={{ backgroundColor: '#3B5998' }}>
								<Icon name="sc-telegram" type="EvilIcons" style={{color:'#fff'}} />
							</Button>
							<Button style={{ backgroundColor: '#DD5144' }}>
								<Icon name="instagram" type="AntDesign" style={{color:'#fff'}} />
							</Button>
						</Fab>
					</ImageBackground>
					<View style={{paddingHorizontal:10,paddingVertical:10}}>
						<Grid>
							<Col>
								<Title style={{textAlign:'right'}}>
									ساعت هوشمند گارمین
								</Title>
							</Col>
						</Grid>
						<Grid>
							<Col>
								<Text note style={{textAlign:'right'}}>
									مدل forerunner945
								</Text>
							</Col>
						</Grid>
					</View>
					<View style={{padding:5,backgroundColor:'#eee'}}>
						<Grid style={{direction:'rtl',marginVertical:10}}>
							<Col style={{paddingHorizontal:5}}>
								<Button full iconLeft style={{backgroundColor:'white'}} onPress={()=>this.props.navigation.navigate('Specifications')}>
									<Icon style={{color:'dark'}} name="filetext1" type="AntDesign" />
									<Text style={{fontSize:13,color:'dark'}}>مشخصات محصول</Text>
								</Button>
							</Col>
							<Col style={{paddingHorizontal:5}}>
								<Button full iconLeft style={{backgroundColor:'white'}} onPress={()=>this.props.navigation.navigate('Filter')}>
									<Icon style={{color:'dark'}} name="comment-discussion" type="Octicons" />
									<Text style={{fontSize:13,color:'dark'}}>نظرات کاربران</Text>
								</Button>
							</Col>
						</Grid>
						<Grid style={{backgroundColor:'white',marginHorizontal:5,}}>
							<Col>
								<ModalSelector
									data={color}
									initValue="انتخاب رنگ"
									style={[styles.ModalSelector,{}]}
									initValueTextStyle={{color:'#000'}}
									cancelText="انصراف"
								/>
							</Col>
							<Col style={{paddingVertical:10}}>
								<Title style={{textAlign:'center',padding:10}}>
									رنگ
									<Text note style={{marginHorizontal:10,textAlign:'right'}}>(۳رنگ)</Text>
								</Title>
							</Col>
						</Grid>
						<Divider/>
						<Grid style={{backgroundColor:'white',marginHorizontal:5,}}>
							<Col>
								<ModalSelector
									data={size}
									initValue="انتخاب سایز"
									style={[styles.ModalSelector,{}]}
									initValueTextStyle={{color:'#000'}}
									cancelText="انصراف"
								/>
							</Col>
							<Col style={{paddingVertical:10}}>
								<Title style={{textAlign:'center',padding:10}}>
									سایز
									<Text note style={{marginHorizontal:10}}>(۴سایز)</Text>
								</Title>
							</Col>
						</Grid>
						<Divider/>
						<List style={{direction:'rtl',backgroundColor:'white',marginHorizontal:5,}}>
							<ListItem>
								<Text note style={{textAlign:'right'}}>گارانتی : <Text>۱۸ ماهه</Text></Text>
							</ListItem>
							<ListItem>
								<Text note style={{textAlign:'right'}}>فروشنده : <Text>یادمه</Text></Text>
							</ListItem>
							<ListItem>
								<Text note style={{textAlign:'right'}}>آماده ارسال از انبار یادمه</Text>
							</ListItem>
						</List>
						<Grid style={{backgroundColor:'white',marginHorizontal:5,}}>
							<Col style={{paddingVertical:10}}>
								<Text style={{textAlign:'center',color:'green',fontWeight:'bold'}}>
									۱,۶۶۹,۰۰۰ تومان
								</Text>
							</Col>
							<Col style={{paddingVertical:10}}>
								<Text style={{textAlign:'center',textDecorationLine:'line-through',color:'red'}}>
									۱,۷۹۹,۰۰۰ تومان
								</Text>
							</Col>
						</Grid>
					</View>
				</Card>
                </Content>
				<Footer>
					<FooterTab>
						<Button full success>
							<Text style={{color:'white',fontWeight:'bold'}}>
								اظافه کردن به سبد خرید
							</Text>
						</Button>
					</FooterTab>
				</Footer>
			</Container>
		);
	}
}

const styles = StyleSheet.create({
	TitleStyle:{
		fontWeight:'bold',
		color:'#4a3199',
	},
	TextStyle:{
		fontSize:11.5,
		color:'#b361a0',
	},
	HeaderStyle:{
		width:'100%',
		height:350,
		top:5,
	},
	ModalSelector:{
        marginHorizontal:30,
        marginVertical:10,
	},
	ModalSelector:{
		marginVertical:5,
		marginHorizontal:5,
		direction: 'ltr',
	},
	colorStyle:{
		borderRadius:10,
		padding:5
	},
	textColorStyle:{
		color: 'white',
		textAlign:'center'
	},
})

export {ProductDetailsComponent}