
import React, {Component} from 'react';
import {StyleSheet, Image, ImageBackground, View, } from 'react-native';
import {CheckBox} from 'react-native-elements';
import {Container, Header, Left, Body, Right, Icon, Title, Text, Content, Footer, FooterTab, Col, Button, Grid, Card, ListItem} from 'native-base';
import Modal from "react-native-modal";
import ModalSelector from 'react-native-modal-selector';

class ProductListComponent extends Component {
	state = {
		isModalVisible: false
	};
	toggleModal = () => {
	this.setState({ isModalVisible: !this.state.isModalVisible });
	};
	state = {
		isModalVisible1: false
	};
	toggleModal1 = () => {
	this.setState({ isModalVisible1: !this.state.isModalVisible1 });
	};
	constructor(props){
		super(props);
		this.state={
		  	textInputValue: ''
		}
	}
	render() {
		let index = 0;
		const gender=[
			{ key: index++, label: 'آقا'},
            { key: index++, label: 'خانوم'},
		];
        const character = [
            { key: index++, label: 'خانه دار'},
            { key: index++, label: 'علاقه مند به آشپزی'},
            { key: index++, label: 'ورزشکار'},
            { key: index++, label: 'مد و فشن'},
            { key: index++, label: 'مذهبی'},
            { key: index++, label: 'اهل تزئینات منزل'},
            { key: index++, label: 'خلاق'},
            { key: index++, label: 'اهل فیلم'},
            { key: index++, label: 'اهل بازی'},
            { key: index++, label: 'اهل هنر'},
        ];
        const Job = [
            { key: index++, label: 'پزشک'},
            { key: index++, label: 'آرتیست'},
            { key: index++, label: 'نقاش'},
            { key: index++, label: 'گرافیست'},
            { key: index++, label: 'تستی'}
        ];
        const birthmonth = [
            { key: index++, label: 'فروردین'},
            { key: index++, label: 'اردیبهشت'},
            { key: index++, label: 'خرداد'},
            { key: index++, label: 'تیر'},
            { key: index++, label: 'مرداد'},
            { key: index++, label: 'شهریور'},
            { key: index++, label: 'مهر'},
            { key: index++, label: 'آبان'},
            { key: index++, label: 'آذر'},
            { key: index++, label: 'دی'},
            { key: index++, label: 'بهمن'},
            { key: index++, label: 'اسفند'},
        ];
        const agerange = [
            { key: index++, label: '۱ تا ۵ سال'},
            { key: index++, label: '۵ تا ۱۰ سال'},
            { key: index++, label: '۱۰ تا ۱۵ سال'},
            { key: index++, label: '۱۵ تا ۲۰ سال'},
            { key: index++, label: '۲۰ تا ۲۵ سال'},
            { key: index++, label: '۲۵ تا ۳۰ سال'},
            { key: index++, label: '۳۰ تا ۴۰ سال'},
            { key: index++, label: '۴۰ تا ۵۰ سال'},
            { key: index++, label: '۵۰ تا ۶۰ سال'},
            { key: index++, label: '۶۰ تا ۷۰ سال'},
            { key: index++, label: '۷۰ تا ۸۰ سال'},
        ];
        const amount = [
            { key: index++, label: 'تا ۵۰ هزار تومان'},
            { key: index++, label: '۵۰ تا ۱۰۰ هزار تومان'},
            { key: index++, label: '۱۰۰ تا ۱۵۰ هزار تومان'},
            { key: index++, label: '۱۵۰ تا ۲۰۰ هزار تومان'},
            { key: index++, label: '۲۰۰ تا ۳۰۰ هزار تومان'},
            { key: index++, label: '۳۰۰ تا ۴۰۰ هزار تومان'},
            { key: index++, label: '۴۰۰ تا ۵۰۰ هزار تومان'},
            { key: index++, label: '۵۰۰ هزار تومان به بالا'},
        ];
        const occasion = [
            { key: index++, label: 'تولد'},
            { key: index++, label: 'ازدواج'},
            { key: index++, label: 'ولنتاین'},
            { key: index++, label: 'روز معلم'},
            { key: index++, label: 'خیریه برای سلامتی'},
            { key: index++, label: 'بی مناسبت'},
            { key: index++, label: 'روز عشق'},
            { key: index++, label: 'جشن نوروز'},
            { key: index++, label: 'جشن کریسمس'},
        ];
		return (
			<Container>
				<Header>
					<Left>
						<Button transparent onPress={()=>this.props.navigation.goBack()}>
							<Icon style={{color:'#b361a0'}} name="arrowleft" size={30} type="AntDesign"/>
						</Button>
					</Left>
					<Body>
						<Title style={[styles.TitleStyle,{}]}>سامانه آنلاین</Title>
						<Text style={[styles.TextStyle,{}]}>سفارش هدیه و گل در ایران</Text>
					</Body>
					<Right>
						<Image style={{width:50,height:45,}} source={require('../img/logo/logooo.png')}/>
					</Right>
				</Header>
				<Content>
					<View>
						<Button small full iconRight title="Show modal" onPress={this.toggleModal} style={[styles.ShowButtonStyle]}>
							<Text>اطلاعات مخاطب</Text>
							<Icon name="ios-search" type="Ionicons" style={[styles.SearchIconStyle]} />
						</Button>
						<Modal isVisible={this.state.isModalVisible}>
							<View style={[styles.ViewModal,{}]}>
								<View style={[styles.ViewHeaderStyle,{}]}>
									<Button transparent onPress={this.toggleModal}>
										<Icon style={[styles.CloseIconStyle,{}]} name="close" type="AntDesign"/>
									</Button>
								</View>
								<View style={[styles.ViewModalSelector,{}]}>
									<ModalSelector
										data={gender}
										style={styles.ModalSelector}
										initValue="انتخاب جنسیت"
										cancelText="انصراف"
									/>
									<ModalSelector
										data={character}
										style={styles.ModalSelector}
										initValue="انتخاب شخصیت"
										cancelText="انصراف"
									/>
									<ModalSelector
										data={Job}
										style={styles.ModalSelector}
										initValue="انتخاب شغل"
										cancelText="انصراف"
									/>
									<ModalSelector
										data={birthmonth}
										style={styles.ModalSelector}
										initValue="انتخاب ماه تولد"
										cancelText="انصراف"
									/>
									<ModalSelector
										data={agerange}
										style={styles.ModalSelector}
										initValue="انتخاب محدودیت سنی"
										cancelText="انصراف"
									/>
									<ModalSelector
										data={amount}
										style={styles.ModalSelector}
										initValue="انتخاب میزان مبلغ هدیه"
										cancelText="انصراف"
									/>
									<ModalSelector
										data={occasion}
										style={styles.ModalSelector}
										initValue="انتخاب مناسبت هدیه"
										cancelText="انصراف"
									/>
								</View>
								<Button full small title="Hide modal" onPress={this.toggleModal} style={{borderRadius:20,margin:5,backgroundColor:'green'}}>
									<Text>جستجو</Text>
								</Button>
							</View>
						</Modal>
					</View>
					<View>
						<Grid>
							<Col style={{margin:5}}>
								<Card style={{borderRadius:20}}>
									<Image style={{width:150,height:150,borderRadius:20,marginLeft:15}} source={require('../img/product/perfume-1.png')}/>
									<Button style={{borderRadius:20,margin:5,backgroundColor:'#88296e'}} full small onPress={()=>this.props.navigation.navigate('ProductDetails')}>
										<Text>مشاهده</Text>
									</Button>
								</Card>
							</Col>
							<Col style={{margin:5}}>
								<Card style={{borderRadius:20}}>
									<Image style={{width:150,height:150,borderRadius:20,marginLeft:15}} source={require('../img/product/perfume-2.png')}/>
									<Button style={{borderRadius:20,margin:5,backgroundColor:'#88296e'}} full small onPress={()=>this.props.navigation.navigate('ProductDetails')}>
										<Text>مشاهده</Text>
									</Button>
								</Card>
							</Col>
						</Grid>
						<Grid>
							<Col style={{margin:5}}>
								<Card style={{borderRadius:20}}>
									<Image style={{width:150,height:150,borderRadius:20,marginLeft:15}} source={require('../img/product/perfume-3.png')}/>
									<Button style={{borderRadius:20,margin:5,backgroundColor:'#88296e'}} full small onPress={()=>this.props.navigation.navigate('ProductDetails')}>
										<Text>مشاهده</Text>
									</Button>
								</Card>
							</Col>
							<Col style={{margin:5}}>
								<Card style={{borderRadius:20}}>
									<Image style={{width:150,height:150,borderRadius:20,marginLeft:15}} source={require('../img/product/perfume-4.png')}/>
									<Button style={{borderRadius:20,margin:5,backgroundColor:'#88296e'}} full small onPress={()=>this.props.navigation.navigate('ProductDetails')}>
										<Text>مشاهده</Text>
									</Button>
								</Card>
							</Col>
						</Grid>
						<Grid>
							<Col style={{margin:5}}>
								<Card style={{borderRadius:20}}>
									<Image style={{width:150,height:150,borderRadius:20,marginLeft:15}} source={require('../img/product/perfume-5.png')}/>
									<Button style={{borderRadius:20,margin:5,backgroundColor:'#88296e'}} full small onPress={()=>this.props.navigation.navigate('ProductDetails')}>
										<Text>مشاهده</Text>
									</Button>
								</Card>
							</Col>
							<Col style={{margin:5}}>
								<Card style={{borderRadius:20}}>
									<Image style={{width:150,height:150,borderRadius:20,marginLeft:15}} source={require('../img/product/perfume-6.png')}/>
									<Button style={{borderRadius:20,margin:5,backgroundColor:'#88296e'}} full small onPress={()=>this.props.navigation.navigate('ProductDetails')}>
										<Text>مشاهده</Text>
									</Button>
								</Card>
							</Col>
						</Grid>
					</View>
				</Content>
				<Footer>
					<FooterTab>
						<Button>
							<Icon name="user" type="AntDesign" style={[styles.IconStyle,{}]}/>
						</Button>
					</FooterTab>
					<FooterTab>
						<Button>
							<Icon name="hearto" type="AntDesign" style={[styles.IconStyle,{}]}/>
						</Button>
					</FooterTab>
					<FooterTab>
						<Button>
							<Icon name="search1" type="AntDesign" style={[styles.IconStyle,{}]}/>
						</Button>
					</FooterTab>
					<FooterTab>
						<Button>
							<Icon name="home" type="AntDesign" style={[styles.IconStyle,{}]}/>
						</Button>
					</FooterTab>
				</Footer>
			</Container>
		);
	}
}

const styles = StyleSheet.create({
	TitleStyle:{
		fontWeight:'bold',
		color:'#4a3199',
	},
	TextStyle:{
		fontSize:11.5,
		color:'#b361a0',
	},
	HeaderStyle:{
		width:'100%',
		height:180,
		top: 10,
	},
	FormStyle:{
		width:'100%',
		direction:'rtl',
		textAlign:'right',
		paddingHorizontal:30,
		top: 10,
	},
	ItemStyle:{
		marginVertical:5,
		paddingHorizontal:15,
	},
	InputStyle:{
		textAlign:'right',
	},
	TextFormStyle:{
		textAlign:'center',
		paddingVertical:10,
		fontWeight:'bold',
		color:'#fff',
		marginTop: 30,
		backgroundColor:'#b361a0'
	},
	ShowButtonStyle:{
		backgroundColor:'#b361a0',
		margin:5,
		borderRadius:10
	},
	SearchIconStyle:{
		color:'#FFF',
		fontSize:20
	},
	ModalSelector:{
        marginHorizontal:30,
        marginVertical:10,
	},
	ViewModal:{
		backgroundColor:'#fff',
		borderRadius:20
	},
	ViewHeaderStyle:{
		width:'100%',
		backgroundColor:'#b361a0',
		borderTopLeftRadius:20,
		borderTopRightRadius:20
	},
	CloseIconStyle:{
		color:'#fff',
		fontSize:25,
		marginLeft:10,
	},
	ViewModalSelector:{
		justifyContent:'space-around',
		padding:10,
		marginTop:9
	},
	IconStyle:{
		color:'#88296e',
		fontSize:30
	}
})

export {ProductListComponent}