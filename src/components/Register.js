
import React, {Component} from 'react';
import {StyleSheet, Image, ImageBackground, View, } from 'react-native';
import {} from 'react-native-elements';
import {Container, Header, Left, Body, Button, Right, Icon, Title, Text, Content, Footer, FooterTab, Input, Form, Item,} from 'native-base';
import ModalSelector from 'react-native-modal-selector';
import { ProgressSteps, ProgressStep, } from 'react-native-progress-steps';


class RegisterComponent extends Component {
	constructor(props) {
        super(props);
        this.state = {
            textInputValue: ''
        }
    }
	onValueChange2(value: string) {
		this.setState({
			selected2: value
		});
	}
	render() {
		let index = 0;
		const data = [
            { key: index++, label: 'آقا'},
            { key: index++, label: 'خانوم'},
        ];
        const data1 = [
            { key: index++, label: 'تهران'},
            { key: index++, label: 'مشهد'},
            { key: index++, label: 'شیراز'},
            { key: index++, label: 'اصفهان'},
            { key: index++, label: 'تبریز'}
        ];
        const data2 = [
            { key: index++, label: 'تهران'},
            { key: index++, label: 'تهران'},
            { key: index++, label: 'تهران'},
        ];
		return (
			<Container>
				<Header>
					<Left>
						<Button transparent onPress={()=>this.props.navigation.goBack()}>
							<Icon style={{color:'#b361a0'}} name="arrowleft" size={30} type="AntDesign"/>
						</Button>
					</Left>
					<Body>
						<Title style={[styles.TitleStyle,{}]}>سامانه آنلاین</Title>
						<Text style={[styles.TextStyle,{}]}>سفارش هدیه و گل در ایران</Text>
					</Body>
					<Right>
						<Image style={{width:50,height:45,}} source={require('../img/logo/logooo.png')}/>
					</Right>
				</Header>
				<Content>
					<ImageBackground source={require('../img/register-login.png')} style={[styles.HeaderStyle,{}]}></ImageBackground>
					<View style={[{direction: 'rtl',}]}>
						<Text style={[styles.TextFormStyle,{}]}>
							فرم ثبت نام
						</Text>
						<ProgressSteps
							style={[styles.ProgressStepsStyle]}
							borderWidth
						>
							<ProgressStep 
								nextBtnText="بعدی"
								nextBtnTextStyle={buttonTextStyle}
								nextBtnStyle={buttonStyle}
								
							>
								<Form style={[styles.FormStyle,{marginBottom:5}]}>
									<Item rounded last bordered style={[styles.ItemStyle,{}]}>
										<Input rounded placeholder="نام" style={[styles.InputStyle,{}]}/>
									</Item>
									<Item rounded last style={[styles.ItemStyle,{}]}>
										<Input rounded placeholder="نام خانوادگی" style={[styles.InputStyle,{}]}/>
									</Item>
								</Form>
								<ModalSelector
									rounded
									data={data}
									initValue="جنسیت"
									style={[styles.ModalSelector]}
									initValueTextStyle={{color:'#000'}}
									cancelText="انصراف"
								/>
							</ProgressStep>
							<ProgressStep 
								previousBtnText="قبلی" 
								nextBtnText="بعدی"
								nextBtnTextStyle={buttonTextStyle}
								previousBtnTextStyle={buttonTextStyle}
								nextBtnStyle={buttonStyle}
								previousBtnStyle={buttonStyle}
							>
								<View>
									<Form style={[styles.FormStyle,{}]}>
										<Item rounded style={[styles.ItemStyle,{}]}>
											<Input placeholder="ایمیل" style={[styles.InputStyle,{}]}/>
										</Item>
										<Item rounded style={[styles.ItemStyle,{}]}>
											<Input placeholder="تلفن همراه" style={[styles.InputStyle,{}]}/>
										</Item>
									</Form>
								</View>
							</ProgressStep>
							<ProgressStep 
								previousBtnText="قبلی" 
								nextBtnText="بعدی"
								nextBtnTextStyle={buttonTextStyle} 
								previousBtnTextStyle={buttonTextStyle}
								nextBtnStyle={buttonStyle}
								previousBtnStyle={buttonStyle}
							>
								<ModalSelector
									data={data1}
									initValue="استان"
									style={[styles.ModalSelector,{marginVertical:10,}]}
									initValueTextStyle={{color:'#000'}}
									cancelText="انصراف"
								/>
								<ModalSelector
									data={data2}
									initValue="شهر"
									style={[styles.ModalSelector,{marginVertical:10,}]}
									initValueTextStyle={{color:'#000'}}
									cancelText="انصراف"
								/>
							</ProgressStep>
							<ProgressStep 
								previousBtnText="قبلی" 
								finishBtnText="ثبت"
								nextBtnTextStyle={buttonTextStyle}
								previousBtnTextStyle={buttonTextStyle}
								nextBtnStyle={buttonnnStyle}
								previousBtnStyle={buttonStyle}
							>
								<View>
									<Form style={[styles.FormStyle,{}]}>
										<Item rounded style={[styles.ItemStyle,{}]}>
											<Input placeholder="رمز عبور" style={[styles.InputStyle,{}]}/>
										</Item>
										<Item rounded style={[styles.ItemStyle,{}]}>
											<Input placeholder="تکرار رمز عبور" style={[styles.InputStyle,{}]}/>
										</Item>
										<Item rounded style={[styles.ItemStyle,{}]}>
											<Input placeholder="کد معرف" style={[styles.InputStyle,{}]}/>
										</Item>
									</Form>
								</View>
							</ProgressStep>
						</ProgressSteps>
					</View>
				</Content>
				<Footer>
					<FooterTab>
						<Button>
							<Title style={{color:'#000'}}>در صورتی که قبلا ثبت نام کرده اید <Text style={{color:'red',fontWeight:'bold',}}>وارد</Text> شوید</Title>
						</Button>
					</FooterTab>
				</Footer>
			</Container>
		);
	}
}


const styles = StyleSheet.create({
	TitleStyle:{
		fontWeight:'bold',
		color:'#4a3199',
	},
	TextStyle:{
		fontSize:11.5,
		color:'#b361a0',
	},
	HeaderStyle:{
		width:'100%',
		height:180,
		top: 10,
	},
	ProgressStepsStyle:{
		direction:'rtl',
	},
	FormStyle:{
		width:'100%',
		direction:'rtl',
		textAlign:'right',
		paddingHorizontal:30
	},
	ItemStyle:{
		marginVertical:5,
		paddingHorizontal:15
	},
	InputStyle:{
		textAlign:'right'
	},
	TextFormStyle:{
		textAlign:'center',
		paddingVertical:10,
		fontWeight:'bold',
		color:'#fff',
		marginTop: 30,
		backgroundColor:'#b361a0'
	},
	ModalSelector:{
		marginHorizontal:30,
		direction:'ltr'
    }
})
const buttonTextStyle={
	color:'#fff',
}
const buttonStyle={
	backgroundColor:'#b361a0',
	borderRadius:10,
	paddingHorizontal:20,
	paddingTop:2,
}
const buttonnnStyle={
	backgroundColor:'green',
	borderRadius:10,
	paddingHorizontal:20,
	paddingTop:2,
}

export {RegisterComponent}