
import React, {Component} from 'react';
import {StyleSheet, Image, View, } from 'react-native';
import { Avatar,Divider } from 'react-native-elements';
import {Container, Header, Left, Body, Right, Icon, Title, Text, Content, Footer, Grid, Col, Item, Button} from 'native-base';
import ModalSelector from 'react-native-modal-selector';

class ProfileComponent extends Component {
	render() {
		return (
			<Container>
				<Header style={{backgroundColor:'#b361a0'}}>
                    <Avatar
                        rounded
                        source={require('../img/user.png')}
                        size="large"
                    />
                </Header>
                <Content style={styles.Content}>
                    <View style={[styles.ViewStyle,{padding:5,margin:5,backgroundColor:'#eee',borderRadius:5}]}>
                        <Grid style={styles.GridPanel}>
                            <Col>
                                <Text style={styles.UserName}>نام و نام خانوادگی :</Text>
                            </Col>
                            <Col>
                                <Text style={styles.UserName}>نام و نام خانوادگی</Text>
                            </Col>
                        </Grid>
                        <Grid style={styles.GridPanel}>
                            <Col>
                                <Text style={styles.UserName}>شماره همراه :</Text>
                            </Col>
                            <Col>
                                <Text style={styles.UserName}>۰۹۱۲۳۴۵۶۷۸۹</Text>
                            </Col>
                        </Grid>
                        <Grid style={styles.GridPanel}>
                            <Col>
                                <Text style={styles.UserName}>ایمیل :</Text>
                            </Col>
                            <Col>
                                <Text style={styles.UserName}>admin@admin.com</Text>
                            </Col>
                        </Grid>
                        <Grid style={styles.GridPanel}>
                            <Col>
                                <Text style={styles.UserName}>شماره کارت : </Text>
                            </Col>
                            <Col>
                                <Text style={styles.UserName}>۱۲۳۴۵۶۷۸۹۸۷۶۵۴۳۲</Text>
                            </Col>
                        </Grid>
                        <Grid style={styles.GridPanel}>
                            <Col>
                                <Text style={styles.UserName}>کد ملی : </Text>
                            </Col>
                            <Col>
                                <Text style={styles.UserName}>۰۰۱۲۹۸۴۹۳۰</Text>
                            </Col>
                        </Grid>
                        <Grid>
                            <Col>
                                <Button iconLeft danger block small style={{margin:5}} onPress={()=>this.props.navigation.goBack()}>
                                    <Icon name="arrowright" type="AntDesign" />
                                    <Text>بازگشت</Text>
                                </Button>
                            </Col>
                            <Col>
                                <Button iconLeft success block small style={{margin:5}}>
                                    <Icon name="update" type="MaterialCommunityIcons" />
                                    <Text>بروزرسانی</Text>
                                </Button>
                            </Col>
                        </Grid>
                    </View>
                    <View style={styles.ViewStyle}>
                        <Grid>
                            <Col style={styles.ColPanel}>
                                <View style={styles.ViewPanel}>
                                    <Image style={styles.ImagePanel} source={require('../img/panel/curriculumside.png')}/>
                                    <Button small block bordered danger style={styles.ButtonPanel}>
                                        <Text>ویرایش اطلاعات</Text>
                                    </Button>
                                </View>
                            </Col>
                            <Col style={styles.ColPanel}>
                                <View style={styles.ViewPanel}>
                                    <Image style={styles.ImagePanel} source={require('../img/panel/resetside.png')}/>
                                    <Button small block bordered danger style={styles.ButtonPanel}>
                                        <Text>تغییر رمز عبور</Text>
                                    </Button>
                                </View>
                            </Col>
                        </Grid>
                        <Grid>
                            <Col style={styles.ColPanel}>
                                <View style={styles.ViewPanel}>
                                    <Image style={styles.ImagePanel} source={require('../img/panel/walletside.png')}/>
                                    <Button small block bordered danger style={styles.ButtonPanel}>
                                        <Text>شارژ کیف پول</Text>
                                    </Button>
                                </View>
                            </Col>
                            <Col style={styles.ColPanel}>
                                <View style={styles.ViewPanel}>
                                    <Image style={styles.ImagePanel} source={require('../img/panel/loveside.png')}/>
                                    <Button small block bordered danger style={styles.ButtonPanel}>
                                        <Text>مجصولات محبوب</Text>
                                    </Button>
                                </View>
                            </Col>
                        </Grid>
                        <Grid>
                            <Col style={styles.ColPanel}>
                                <View style={styles.ViewPanel}>
                                    <Image style={styles.ImagePanel} source={require('../img/panel/billside.png')}/>
                                    <Button small block bordered danger style={styles.ButtonPanel}>
                                        <Text>فاکتورها</Text>
                                    </Button>
                                </View>
                            </Col>
                            <Col style={styles.ColPanel}>
                                <View style={styles.ViewPanel}>
                                    <Image style={styles.ImagePanel} source={require('../img/panel/mapside.png')}/>
                                    <Button small block bordered danger style={styles.ButtonPanel}>
                                        <Text>ثبت آدرس ها</Text>
                                    </Button>
                                </View>
                            </Col>
                        </Grid>
                        <Grid>
                            <Col style={styles.ColPanel}>
                                <View style={styles.ViewPanel}>
                                    <Image style={styles.ImagePanel} source={require('../img/panel/calendarside.png')}/>
                                    <Button small block bordered danger style={styles.ButtonPanel}>
                                        <Text>تاریخ های مناسبت ها</Text>
                                    </Button>
                                </View>
                            </Col>
                            <Col style={styles.ColPanel}>
                                <View style={styles.ViewPanel}>
                                    <Image style={styles.ImagePanel} source={require('../img/panel/ticketsside.png')}/>
                                    <Button small block bordered danger style={styles.ButtonPanel}>
                                        <Text>تیکت ها</Text>
                                    </Button>
                                </View>
                            </Col>
                        </Grid>
                    </View>
                </Content>
			</Container>
		);
	}
}

const styles = StyleSheet.create({
    Content:{
        paddingTop: 40,
    },
    ViewStyle:{
        direction:'rtl'
    },
    GridPanel:{
        marginVertical:5,
        paddingHorizontal:10,
    },
    UserName:{
        textAlign:'left',
        color:'#b361a0',
        fontWeight:'bold',
        fontSize:13
    },
    ColPanel:{
        padding:5
    },
    ViewPanel:{
        backgroundColor:'#eee',
        borderRadius:5,
        padding:5
    },
    ImagePanel:{
        width:65,
        height:65,
        marginHorizontal:45,
    },
    ButtonPanel:{
        marginTop:10
    },
})

export {ProfileComponent}