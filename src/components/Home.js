
import React, {Component} from 'react';
import {StyleSheet, Image,} from 'react-native';
import { Divider } from 'react-native-elements';
import {Container, Content, Drawer, Header, Footer, Text, Left, Body, Right, Button, Icon, View, FooterTab, Grid, Col, Title, Card, Form, Item, Input,}from 'native-base';
import {SideBar} from './index';
import ModalSelector from 'react-native-modal-selector';
import { ProgressSteps, ProgressStep, } from 'react-native-progress-steps';

class Home extends Component{
	constructor(props) {
		super(props);
        this.state = {
            textInputValue: ''
        }
	}
	closeDrawer=()=>{
		this.drawer._root.close()
	}
	openDrawer=()=>{
		this.drawer._root.open()
	}
	render() {
		const {navigate}=this.props.navigation;
		let index = 0;
		const data = [
            { key: index++, label: 'تستی'},
            { key: index++, label: 'تستی'},
            { key: index++, label: 'تستی'}
        ];
        const data1 = [
            { key: index++, label: 'تهران'},
            { key: index++, label: 'مشهد'},
            { key: index++, label: 'شیراز'},
            { key: index++, label: 'اصفهان'},
            { key: index++, label: 'تبریز'}
        ];  
		return (
			<Drawer
				ref={(ref)=>{this.drawer=ref;}}
				content={<SideBar navigate={navigate}/>}
				onClose={()=>this.closeDrawer()}
			>
				<Container >
					<Header searchBar rounded style={[styles.HeaderStyle,{}]}>
						<Left>
							<Button transparent onPress={()=>this.openDrawer()}>
								<Icon name="navicon" type="FontAwesome" style={[styles.DerawerStyle]}/>
							</Button>
						</Left>
						<Body>
							<Title style={[styles.TitleStyle,{}]}>سامانه آنلاین</Title>
							<Text style={[styles.TextStyle,{}]}>سفارش هدیه و گل در ایران</Text>
						</Body>
						<Right>
							<Image style={{width:50,height:45,}} source={require('../img/logo/logooo.png')}/>
						</Right>
					</Header>
					<Content style={styles.ContentStyle}>
						<View style={[{direction:'rtl',borderWidth:2,borderColor:'#b361a0',margin:5,borderRadius:20,paddingHorizontal:10}]}>
							<Text style={[styles.TextFormStyle,{}]}>
								یادآوری مناسبت های عزیزان شما		
							</Text>
							<ProgressSteps
								style={[styles.ProgressStepsStyle,{width:'100%',paddingVertical:0,borderWidth:2,borderColor:'#88296e',}]}
								borderWidth
							>
								<ProgressStep 
									nextBtnText="بعدی"
									nextBtnTextStyle={buttonTextStyle}
									nextBtnStyle={buttonStyle}
								>
									<Text style={[styles.TextProgressStepStyle,{}]}>
										اطلاعات فردی شما
									</Text>
									<Form>
										<Item rounded last bordered style={[styles.ItemStyle,{}]}>
											<Input rounded placeholder="نام" style={[styles.InputStyle,{}]}/>
										</Item>
										<Item rounded last style={[styles.ItemStyle,{}]}>
											<Input rounded placeholder="نام خانوادگی" style={[styles.InputStyle,{}]}/>
										</Item>
										<Item rounded last style={[styles.ItemStyle,{}]}>
											<Input rounded placeholder="شماره همراه" style={[styles.InputStyle,{}]}/>
										</Item>
									</Form>
								</ProgressStep>
								<ProgressStep 
									previousBtnText="قبلی" 
									finishBtnText="ثبت"
									nextBtnTextStyle={buttonTextStyle}
									previousBtnTextStyle={buttonTextStyle}
									nextBtnStyle={buttonnnStyle}
									previousBtnStyle={buttonStyle}
								>
									<View>
										<Text style={[styles.TextProgressStepStyle,{}]}>
											اطلاعات مخاطب شما
										</Text>
										<Form>
											<Item rounded last bordered style={[styles.ItemStyle,{}]}>
												<Input rounded placeholder="نام" style={[styles.InputStyle,{}]}/>
											</Item>
											<Item rounded last style={[styles.ItemStyle,{}]}>
												<Input rounded placeholder="نام خانوادگی" style={[styles.InputStyle,{}]}/>
											</Item>
											<Item rounded last style={[styles.ItemStyle,{}]}>
												<Input rounded placeholder="شماره همراه" style={[styles.InputStyle,{}]}/>
											</Item>
										</Form>
										<ModalSelector
											rounded
											data={data}
											initValue="نسبت با شما"
											style={[styles.ModalSelector,{direction: 'ltr',}]}
											initValueTextStyle={{color:'#000'}}
											cancelText="انصراف"
										/>
										<ModalSelector
											data={data1}
											initValue="شغل"
											style={[styles.ModalSelector,{direction: 'ltr',}]}
											initValueTextStyle={{color:'#000'}}
											cancelText="انصراف"
										/>
									</View>
								</ProgressStep>
							</ProgressSteps>
						</View>
						<Divider style={{marginTop:-10}}/>
						<View style={[styles.ViewStyle,{}]}>
							<Grid style={[styles.GridStyle,{}]}>
								<Col size={50}>
									<View style={[styles.ViewGridStyle,{}]}>
										<Image source={require('../img/icon/women-icon.png')} style={[styles.ImageCardStyle,{}]}/>
										<Button full transparent small onPress={()=>this.props.navigation.navigate('Filter')}>
											<Text style={[styles.TextCardStyle,{}]}>
												خانوم ها
											</Text>
										</Button>
									</View>
								</Col>
								<Col size={50}>
									<View style={[styles.ViewGridStyle,{}]}>
										<Image source={require('../img/icon/men-icon.png')} style={[styles.ImageCardStyle,{}]}/>
										<Button full transparent small onPress={()=>this.props.navigation.navigate('Filter')}>
											<Text style={[styles.TextCardStyle,{}]}>
												آقایان		
											</Text>
										</Button>
									</View>
								</Col>
							</Grid>
							<Grid style={[styles.GridStyle,{}]}>
								<Col size={50}>
									<View style={[styles.ViewGridStyle,{}]}>
										<Image source={require('../img/icon/di.png')} style={[styles.ImageCardStyle,{}]}/>
										<Button full transparent small onPress={()=>this.props.navigation.navigate('Filter')}>
											<Text style={[styles.TextCardStyle,{}]}>
												لاکچری		
											</Text>
										</Button>
									</View>
								</Col>
								<Col size={50}>
									<View style={[styles.ViewGridStyle,{}]}>
										<Image source={require('../img/icon/rose-icon.png')} style={[styles.ImageCardStyle,{}]}/>
										<Button full transparent small onPress={()=>this.props.navigation.navigate('Filter')}>
											<Text style={[styles.TextCardStyle,{}]}>
												گل		
											</Text>
										</Button>
									</View>
								</Col>
							</Grid>
							<Grid style={[styles.GridStyle,{}]}>
								<Col size={50}>
									<View style={[styles.ViewGridStyle,{}]}>
										<Image source={require('../img/icon/flower-icon.png')} style={[styles.ImageCardStyle,{}]}/>
										<Button full transparent small onPress={()=>this.props.navigation.navigate('Filter')}>
											<Text style={[styles.TextCardStyle,{}]}>
												معنوی		
											</Text>
										</Button>
									</View>
								</Col>
								<Col size={50}>
									<View style={[styles.ViewGridStyle,{}]}>
										<Image source={require('../img/icon/gift-icon.png')} style={[styles.ImageCardStyle,{}]}/>
										<Button full transparent small onPress={()=>this.props.navigation.navigate('Filter')}>
											<Text style={[styles.TextCardStyle,{}]}>
												هدیه		
											</Text>
										</Button>
									</View>
								</Col>
							</Grid>
						</View>
					</Content>
					<Footer>
						<FooterTab>
							<Button>
								<Icon name="user" type="AntDesign" style={[styles.IconStyle,{}]}/>
							</Button>
						</FooterTab>
						<FooterTab>
							<Button>
								<Icon name="hearto" type="AntDesign" style={[styles.IconStyle,{}]}/>
							</Button>
						</FooterTab>
						<FooterTab>
							<Button>
								<Icon name="search1" type="AntDesign" style={[styles.IconStyle,{}]}/>
							</Button>
						</FooterTab>
						<FooterTab>
							<Button>
								<Icon name="home" type="AntDesign" style={[styles.IconStyle,{}]}/>
							</Button>
						</FooterTab>
					</Footer>
				</Container>
			</Drawer>
		);
	}
}
const styles = StyleSheet.create({
	HeaderStyle:{
		height:70
	},
	ContentStyle:{
		backgroundColor:'#fff',
	},
	DerawerStyle:{
		color:'#b361a0',
		fontSize:37,
	},
	TitleStyle:{
		fontWeight:'bold',
		color:'#4a3199',
	},
	TextStyle:{
		fontSize:11.5,
		color:'#b361a0',
	},
	BanerStyle:{
		width:360,
		height: 180,
		marginHorizontal:5,
		marginVertical:15, 
		paddingHorizontal:10,
	},
	ItemStyle:{
		marginVertical:5,
		paddingHorizontal:15
	},
	InputStyle:{
		textAlign:'right'
	},
	ViewStyle:{
		paddingVertical:10,
		paddingHorizontal:10,
	},
	ViewGridStyle:{
		backgroundColor:'#fff',
		borderRadius: 15,
		margin:10,
		paddingVertical:20,
		shadowColor: '#000',
		shadowOffset: { width: 0, height: 3 },
		shadowOpacity: 0.3,
		shadowRadius: 4,
	},
	ImageCardStyle:{
		width:120,
		height:100,
		marginHorizontal:17,
	},
	TextCardStyle:{
		textAlign:'center',
		fontWeight:'bold',
		color:'#b361a0',
	},
	IconStyle:{
		color:'#88296e',
		fontSize:30
	},
	TextFormStyle:{
		textAlign:'center',
		paddingVertical:5,
		fontWeight:'bold',
		color:'#b361a0',
		marginTop:0,
	},
	TextProgressStepStyle:{
		color:'#b361a0',
		backgroundColor:'#fff',
		paddingRight:20,
		fontWeight:'bold',
		fontSize:20,
		direction:'ltr',
		textAlign:'right'
	},
	ModalSelector:{
		marginVertical:5,
    }
})
const buttonTextStyle={
	color:'#fff',
}
const buttonStyle={
	backgroundColor:'#b361a0',
	borderRadius:10,
	paddingHorizontal:20,
	paddingTop:2,
}
const buttonnnStyle={
	backgroundColor:'green',
	borderRadius:10,
	paddingHorizontal:20,
	paddingTop:2,
}
export {Home}
