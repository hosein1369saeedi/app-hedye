import React, {Component} from 'react';
import{ StyleSheet,ImageBackground} from 'react-native';
import{ Container, Content, List, ListItem, Button, Text, Icon, Header, Footer, FooterTab, Title, Grid, Col,} from 'native-base';
import{ Avatar, } from 'react-native-elements';

class SideBar extends Component {
	constructor(props){
		super(props) 
	}
	render(){  
		return(
			<Container style={styles.SideBarStyle}>
				<ImageBackground source={require('../img/app-2.png')} style={[styles.HeaderStyle,{}]}>
					<Grid top={90} right={10}>
						<Col size={70} style={[styles.ColStyle,{}]}>
							<Title style={[styles.TextStyle,{}]}>نام و نام خانوادگی</Title>
							<Text style={[styles.TextStyle,{}]}>۰۹۱۲۳۴۵۶۷۸۹</Text>
						</Col>
						<Col size={30}>
							<Avatar
								rounded
								source={require('../img/user.png')}
								size="large"
							/>
						</Col>
					</Grid>
				</ImageBackground>
				<Content>
					<List style={{direction:'rtl'}}>
						<ListItem>
							<Button style={[styles.buttonStyle,{}]} onPress={()=>this.props.navigate('Profile')}>
								<Icon style={[styles.Icon,{}]} name="user" type="SimpleLineIcons"/> 
								<Text style={styles.sideButton}>پروفایل</Text>
							</Button>
						</ListItem>
						<ListItem>
							<Button style={[styles.buttonStyle,{}]} onPress={()=>this.props.navigate('Register')}>
								<Icon style={[styles.Icon,{}]} name="user" type="SimpleLineIcons"/> 
								<Text style={styles.sideButton}>ثبت نام</Text>
							</Button>
						</ListItem>
						<ListItem>
							<Button style={[styles.buttonStyle,{}]} onPress={()=>this.props.navigate('Login')}>
								<Icon style={[styles.Icon,{}]} name="login" type="AntDesign"/>
								<Text style={styles.sideButton}>ورود</Text>
							</Button>
						</ListItem>
						<ListItem>
							<Button style={[styles.buttonStyle,{}]} onPress={()=>this.props.navigate('Contactus')}>
								<Icon style={[styles.Icon,{}]} name="contacts" type="AntDesign"/>
								<Text style={styles.sideButton}>تماس باما</Text>
							</Button>
						</ListItem>
						<ListItem>
							<Button style={[styles.buttonStyle,{}]} onPress={()=>this.props.navigate('Aboutus')}>
								<Icon style={[styles.Icon,{}]} name="exception1" type="AntDesign"/>
								<Text style={styles.sideButton}>درباره ما</Text>
							</Button>
						</ListItem>
						<ListItem>
							<Button style={[styles.buttonStyle,{}]} onPress={()=>this.props.navigate('ProductDetails')}>
								<Icon style={[styles.Icon,{}]} name="setting" type="AntDesign"/>
								<Text style={styles.sideButton}>محصولات</Text>
							</Button>
						</ListItem>
						<ListItem>
							<Button style={[styles.buttonStyle,{}]} onPress={()=>this.props.navigate('Setting')}>
								<Icon style={[styles.Icon,{}]} name="setting" type="AntDesign"/>
								<Text style={styles.sideButton}>تنظیمات</Text>
							</Button>
						</ListItem>
					</List>
				</Content>
				<Footer>
					<FooterTab>
						<ListItem>
							<Button small style={[styles.buttonStyle,{}]}>
								<Text style={[styles.sideButton,{color:'#FFF',fontSize:15,}]}>خروج از حساب</Text>
								<Icon style={[{color:'#fff'}]} name="md-log-out" type="Ionicons"/>
							</Button>
						</ListItem>
					</FooterTab>
				</Footer>
			</Container>
		)
	}
}
const styles = StyleSheet.create({
	SideBarStyle:{
		flex:1,
		backgroundColor:'#EEE',
	},
	ColStyle:{
		padding: 15,
	},
	TextStyle:{
		textAlign:'right',
		color:'#FFF'
	},
	HeaderStyle:{
		width:'100%',
		height:190
	},
	sideButton:{
		fontSize:20,
		color:'#88296e',
		fontWeight:'bold'
	},
	Icon:{
		color:'#b361a0',
	},
	buttonStyle:{
		backgroundColor:'#eee',
	},
})
export {SideBar}